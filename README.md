# TAKO

## Erasmus+ project
This software code is part of the Erasmus+ project "TAKO: an automated design thinking process facilitation proof-of-concept chatbot", project number: 2022-2-NL01-KA210-SCH-000099521. The project aims to harness the potential of task-oriented dialogue systems or chatbot technologies for teaching and learning elements of the design thinking (DT) approach in upper secondary education, a proven framework for innovation and development of empathy, creativity, collaboration, critical cross-disciplinary thinking and problem-solving.

The code is a functional prototype of a chatbot that can be used to facilitate the design thinking process. The chatbot is based on the OpenAI API and uses a long-term memory to store conversations. The chatbot can be used to facilitate the design thinking process in a classroom setting. The chatbot can be used to guide students through the design thinking process, ask questions, and provide feedback. The chatbot stores conversations in a Sqlite3 database which makes it possible to analyze conversations and improve the chatbot. The code is written in Python and uses the Streamlit library to create a web interface. It is quite easy to make changes to the chatbot, for example, by changing the prompts or adding new prompts.

TAKO is available online at: https://tako.doon.nu/ - For now, you do not need an OpenAI key to use this online TAKO version. However, if the costs become too high, we might need to ask for a key. Please also note that this is a prototype and might not always work as expected.

For more information about the project, see https://doon.nu/tako/ or contact info at doon.nu or berry.groenendijk at gmail.com.

## License
This software is licensed under the MIT License. See the LICENSE file for details.
The prompts are licensed under the Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License. See the LICENSE file for details.

## With thanks to...

We'd like to give a big thanks to the following people for making TAKO happen:
- [Prestoj](https://github.com/prestoj/long-term-chat/blob/main/memory.py) for the long-term memory code.

## Build with...
- TAKO is built with Python and Streamlit, see: https://streamlit.io/
- TAKO uses the OpenAI API. You need an OpenAI key to run TAKO. You can get a key at: https://platform.openai.com
- Docker is used to run TAKO in a container. See: https://www.docker.com/

## Run Docker container locally (development)
You can easily run TAKO in a Docker container. This is the preferred way to run TAKO.

- Clone this repository locally.
- Create a file `config/env.list` in the root of the project directory with the following content:
  ```
  OPENAI_KEY=[your OpenAI key]
  ```
- Run the Docker image: `docker compose -f docker-compose.yml up` or `docker compose -f docker-compose.yml up -d`
- To rebuild the image: `docker compose up --build` or `docker compose up --build`

Remarks:
- A Sqlite3 database with conversations is stored in the Docker volume `app-data`
- A Chroma vector database is stored in the Docker volume `app-data`

View chat conversations:
- Chat conversations are stored in a Sqlite3 database.
- You can easily view the database with a tool like: https://datasette.io/
- In a separate terminal, list docker containers: `docker ps`
- Use the container ID of the `tako-web` and copy the sqlite3 database to your local machine: `docker cp [container ID]:/app/data/conversations.db [local path]`
- On your local machine, open the database with Datasette: `datasette [path to database]`

## Create a prompt or modify and existing TAKO prompt
You need to have a bit of Python skills to create your own prompt or to modify an existing one.
- In `config.py` the variable `PROMPT_STYLES` contains references to different prompts.
- The prompt are defined in `prompt.py`. Here you can add your own prompt.
- Prompts are written in specific format called SudoLang. For more information see: https://github.com/paralleldrive/sudolang-llm-support and https://github.com/paralleldrive/sudolang-llm-support/blob/main/sudolang.sudo.md and https://medium.com/javascript-scene/sudolang-a-powerful-pseudocode-programming-language-for-llms-d64d42aa719b
  
## Install Docker in production
I used an Amazon LightSail instance with Ubuntu.

Deploying the website:
- SSH into the server.
- Use an SSH key for easy access to Gitlab. See: https://docs.gitlab.com/ee/user/ssh.html
- Git clone the project: `git clone git@gitlab.com:berryg/tako.git`
- cd `tako`
- - Create a file `config/env.list` in the root of the project directory with the following content:
  ```
  OPENAI_KEY=[your OpenAI key]
  ```
- Create network: `sudo docker network create traefik_network`
- Build the Docker image: `sudo docker compose -f docker-compose-prod.yml build`
- Run the Docker image: `sudo docker compose -f docker-compose-prod.yml up -d`
- Check if the website is running correctly:
  - `sudo docker ps` 
  - Perhaps check the logs: `sudo docker logs [container ID]`

Update website:
- SSH into the server.
- cd `tako`
- Pull the latest version: `git pull`
- Run the Docker image: `sudo docker compose -f docker-compose-prod.yml up --build -d`

### Backup the Sqlite3 database
- `sudo docker cp [container ID]:/app/data/conversations.db ../tako_backups/conversations_YYYYMMDD.db`
- User SCP to copy the file to a local machine: `scp -i ~/.ssh/yourkey.pem ubuntu@yourserver:/path/to/file /path/to/destination`

## Developer notes

### Run TAKO locally without Docker

** Note: ** The following instructions are for running TAKO on a local machine. Running TAKO in a Docker container, is preferred (see above).

Install venv, virtual environment. Necessary to avoid version clashes between libraries...
See also: https://python.land/virtual-environments/virtualenv

- Make sure you install python3-dev: `sudo apt install python3-dev`, it is necessary for building chroma.
- If necessary, install venv first: `sudo apt install python3-venv`
- Create venv: `python -m venv venv`
- Activate venv: `source venv/bin/activate`
- Install libraries: `pip install -r requirements.txt`
- Deactivate venv: `deactivate`

If you run TAKO for the first time, populate sqlite db first:

- Run: `python -m conversation_create_db`

Run TAKO:

- Optionally, set environment variable: `export OPENAI_KEY=[your OpenAI key]`
- Run TAKO: `python -m streamlit run tako.py`
- Run TAKO with minimal toolbar menu: `python -m streamlit run tako.py --client.toolbarMode minimal`. Default: auto.

- To **delete Chroma Vector DB**, just delete the directory /app/data/chromadb.

- **Session state** is not what you expect in Streamlit. On each action in Streamlit the Python code runs again. So, on each run you lose information. To keep data available between each run you need session state. However, if you refresh the app, the session state is cleared. Session state is not persistent in its usual sense.

## Upgrade libraries
- Install pip-review: `pip install pip-review` 
- Run pip-revies: `pip-review --auto` 

## Install TAKO on a Raspberry Pi 4
Here are instructions to install TAKO directly on a Raspberry Pi without using Docker. Later, I started using Docker, which made the installation much easier across different environments. 

### Install Python 3.10 on Raspberry Pi 4
Use PyEnv. See also: https://stackoverflow.com/questions/70422866/how-to-create-a-venv-with-a-different-python-version
  
- Install PyEnv: `curl https://pyenv.run | bash`
- `sudo apt update; sudo apt install build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl libncursesw5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev`
- `pyenv install 3.10`
- Create a venv with python 3.10 in it: `python3.10 -m venv ./venv`
- Activate venv: `source venv/bin/activate`
- Check python version: `python --version`
- Install libraries: `pip install -r requirements.txt`

### Install on Raspberry Pi 4

Follow instructions above for an installation without Docker. Perform the following additional steps:

- Chroma needs Sqlite >= 3.53.
- The solution is to use a specific version of pysqlite that contains the correct sqlite version, see: https://github.com/pysqlite3/pysqlite3/releases/tag/v0.5.1.3380300.
- But, for this to work the ChromaDB **init**py needs to be modified.
- Open init file: `nano /home/berry/tako/venv/lib/python3.9/site-packages/chromadb/__init__.py`
- Just before `if sqlite3.sqlite_version_info < (3, 35, 0):` set `IN_COLAB = True`
- Change line `[sys.executable, "-m", "pip", "install", "pysqlite-binary"]` to `[sys.executable, "-m", "pip", "install", "pysqlite-binary==0.5.1.3380300"]`
- Now `python -m streamlit run tako.py` works.

### Run Tako.py as a service

- See: https://stackoverflow.com/questions/13069634/python-daemon-and-systemd-service and https://wiki.debian.org/systemd/Services
- And executing python in a venv using systemd: https://unix.stackexchange.com/questions/409609/how-to-run-a-command-inside-a-virtualenv-using-systemd and https://stackoverflow.com/questions/37211115/how-to-enable-a-virtualenv-in-a-systemd-service-unit
- Created a tako.service file and copied it in systemd. Location: `/etc/systemd/system`
- Now `sudo systemctl [start|stop|status] tako.service` works and tako is a service on RPi4. Nice!

### Tako available on the internet using Tailscale

- Make sure your RPi4 is part of a Tailscale network - See: https://tailscale.com/
- Using Tailscale Funnel and Serve the app is available on the internet including support for HTTPS.
- Create a funnel using: https://tailscale.com/kb/1223/tailscale-funnel/ and https://tailscale.com/kb/1247/funnel-serve-use-cases/
- Serve Tako using: `sudo tailscale serve https / http://127.0.0.1:8501`
- Check funnel status using: `tailscale funnel status`
- Website is available at: https://raspberrypi4.tail689e5.ts.net

