from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from conversation import Base, Conversation, ShortTermMemory

if __name__ == "__main__":
    engine = create_engine('sqlite:////app/data/conversations.db')
    Session = sessionmaker(bind=engine)
    db = Session()
    with db:
        db.query(Conversation).delete()
        db.query(ShortTermMemory).delete()
        db.commit()
        db.close()
