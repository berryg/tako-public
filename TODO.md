# Known errors
- Reading OPENAI_KEY from UI does not work anymore. It is necessary to set the environment variable OPENAI_KEY.

# Running list of TODO's

- [X] Understand workings of long term memory and fine tune working of it to a Design Thinking Coach. Create logging to get insight into working of the memory.
- [X] Build option to create a new chat
- [X] make TAKO usable for multiple users
- [X] create a "session" per team. And thus also a vector DB collection per team?
- [X] host TAKO on a general available weblocation
- [-] (Joos) Create a nice logo for TAKO.
- [X] translate all system prompts to Dutch. Does that make a difference in how TAKO works? Is that necessary?
- [X] Quickly you run into this error: "This model's maximum context length is 4097 tokens. However, your messages resulted in 4104 tokens. Please reduce the length of the messages." --> implemented Chroma, a vector DB.
- [X] What model is being used? Can I select the model being used?
- [X] Store SudoLang program in a separate Python file.

# Software Design

Chat design:

- one team has multiple users (implement later?)
- one team can have multiple chats (really?)
- or... one team has one chat (= collection). Can multiple people view/interact in this one chat?

# Coding tips

## Building a memory

- [X] Still need to look at: https://medium.com/@simon_attard/building-a-memory-layer-for-gpt-using-function-calling-da17d66920d0 - Function Calling and long term memory. Basically, it uses OpenAI function call to store and retrieve items from a vector db. The decision to store or retrieve an item from memory is made by the LLM. Interesting. It is exactly here that this example deviates from the Prestoj (see below) code. In the Prestoj code the decision to store or retrieve items is made by the Python code. If we, later, want to use different LLMs having the decisions made by the Python code is the better solution.
- Chroma tutorial: https://anderfernandez.com/en/blog/chroma-vector-database-tutorial/
- (must read) Better Long Term Memory: https://github.com/prestoj/long-term-chat/blob/main/memory.py
- Tip/hack: summarize the last 5 interactions continuously. https://twitter.com/JeremyNguyenPhD/status/1674452220706734080
- Chroma Long Term memory: https://lablab.ai/t/chroma-tutorial-with-openais-gpt-35-model-for-memory-feature-in-chatbot
- Chroma vector db: https://medium.com/sopmac-ai/vector-databases-as-memory-for-your-ai-agents-986288530443
- Teach your LLM to always answer with facts not fiction - https://blog.myscale.com/2023/07/17/teach-your-llm-vector-sql/

## Using functions in LLMs

- Do research on the ReAct paper on chain-of-thought reasoning. See: https://arxiv.org/abs/2210.03629
- Open AI functions as a possible way voor LLMs to include information in their reasoning stored elsewhere. See: https://openai-functions.readthedocs.io/en/latest/introduction.html and https://openai.com/blog/function-calling-and-other-api-updates?ref=upstract.com

## LangChain alternatives

- LLM: https://github.com/simonw/llm
- AutoChain: https://autochain.forethought.ai - via: https://news.ycombinator.com/item?id=36775475 - One of the valuable things AutoChain provides is simulated conversation evaluation.

## Open Source LLMs

- (august 2023) Code Llama: https://ai.meta.com/blog/code-llama-large-language-model-coding/ - The Instruct version is perhaps an interesting option for TAKO.
- (june 2023) Claude 2: https://www.anthropic.com/index/claude-2
- (july 2023) Llama 2: https://ai.meta.com/llama/
- (july 2023) Ollama: https://github.com/jmorganca/ollama - "Bundle a model’s weights, configuration, prompts, data and more into self-contained packages that run anywhere." What? TAKO could be bundled into a LLM-package?

## Fine tuning a LLM

- Fine tuning for OpenAI models: https://platform.openai.com/docs/guides/fine-tuning - GTP3/4 finetuning later in 2023.
- Fine tuning Llama2: https://brev.dev/blog/fine-tuning-llama-2
- Article about fine tuning using Llama2: https://www.anyscale.com/blog/fine-tuning-llama-2-a-comprehensive-case-study-for-tailoring-models-to-unique-applications
