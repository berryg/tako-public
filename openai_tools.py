from openai import OpenAI

import config
import tiktoken
import re
import logging
import typing
from conversation import ShortTermMemory

client = OpenAI(api_key=config.OPENAI_KEY)

def get_embedding(text):
    # return a list of embeddings
    embeddings = client.embeddings.create(
        input=[text], model=config.MODEL_EMBEDDING).data[0].embedding
    return embeddings


def get_importance_of_interaction(message, response):
    importance_response = client.chat.completions.create(model='gpt-3.5-turbo',
                                                         messages=[
                                                             {'role': 'system', 'content': 'You are a large language model. The following is a snippet of a conversation between a user and a chatbot. The language used is Dutch.'},
                                                             {'role': 'user',
                                                                 'content': message},
                                                             {'role': 'assistant',
                                                                 'content': response},
                                                             {'role': 'system', 'content': 'Please rate the importance of remembering the above interaction on a scale from 1 to 10 where 1 is trivial and 10 is very important. Only respond with the number, do not add any commentary.'}
                                                         ],
                                                         temperature=0,
                                                         n=1,
                                                         max_tokens=100)

    numbers = re.findall(
        r'\b(?:10|[1-9])\b', importance_response.choices[0].message.content)
    if numbers:
        return int(numbers[0]) / 10

    logging.error(
        "Error: Could not parse importance of interaction. Defaulting to 3 out of 10.")
    return 0.3


def get_importance_of_insight(insight):
    importance_response = client.chat.completions.create(model='gpt-3.5-turbo',
                                                         messages=[
                                                             {'role': 'system', 'content': 'You are a large language model. The following is an insight you gained from of a conversation with a user. The conversation is in Dutch.'},
                                                             {'role': 'assistant',
                                                                 'content': insight},
                                                             {'role': 'system', 'content': 'Please rate the importance of remembering the above insight on a scale from 1 to 10 where 1 is trivial and 10 is very important. Only respond with the number, do not add any commentary.'}
                                                         ],
                                                         temperature=0,
                                                         n=1,
                                                         max_tokens=100)

    numbers = re.findall(
        r'\b(?:10|[1-9])\b', importance_response.choices[0].message.content)
    if numbers:
        return int(numbers[0]) / 10

    logging.error(
        "Error: Could not parse importance of interaction. Defaulting to 3 out of 10.")
    return 0.3


def get_insights(messages):
    response = client.chat.completions.create(model='gpt-3.5-turbo',
                                              messages=messages +
                                              [{'role': 'system', 'content': 'Please list up to 5 high-level insights you can infer from the above conversation. The conversation is in Dutch. You must respond in a list format with each insight surrounded by quotes, e.g. ["The user seems...", "The user likes...", "The user is...", ...]'}],
                                              temperature=0.7,
                                              n=1,
                                              max_tokens=500)

    # Extract the insights from the string
    insights_list = re.findall(r'"(.*?)"', response.choices[0].message.content)

    insights = []

    for insight in insights_list:
        insights.append({
            'content': insight,
            'importance': get_importance_of_insight(insight)
        })

    return insights


# https://github.com/openai/openai-cookbook/blob/main/examples/How_to_count_tokens_with_tiktoken.ipynb
def num_tokens_from_messages(messages, model="gpt-3.5-turbo"):
    """Returns the number of tokens used by a list of messages."""
    try:
        encoding = tiktoken.encoding_for_model(model)
    except KeyError:
        logging.error("Warning: model not found. Using cl100k_base encoding.")
        encoding = tiktoken.get_encoding("cl100k_base")
    if model == "gpt-3.5-turbo":
        return num_tokens_from_messages(messages, model="gpt-3.5-turbo-0301")
    elif model == "gpt-4":
        return num_tokens_from_messages(messages, model="gpt-4-0314")
    elif model == "gpt-3.5-turbo-0301":
        # every message follows <|start|>{role/name}\n{content}<|end|>\n
        tokens_per_message = 4
        tokens_per_name = -1  # if there's a name, the role is omitted
    elif model == "gpt-4-0314":
        tokens_per_message = 3
        tokens_per_name = 1
    else:
        raise NotImplementedError(
            f"""num_tokens_from_messages() is not implemented for model {model}. See https://github.com/openai/openai-python/blob/main/chatml.md for information on how messages are converted to tokens.""")
    num_tokens = 0
    for message in messages:
        num_tokens += tokens_per_message
        for key, value in message.items():
            num_tokens += len(encoding.encode(value))
            if key == "name":
                num_tokens += tokens_per_name
    num_tokens += 3  # every reply is primed with <|start|>assistant<|message|>
    return num_tokens


def list_short_term_memory_messages_for_pruning(messages: typing.List[ShortTermMemory], max_tokens: int, model: str = "gpt-3.5-turbo") -> typing.List[ShortTermMemory]:
    """Returns a list of ShortTermMemory messages for pruning. Code derived from num_tokens_from_messages."""
    try:
        encoding = tiktoken.encoding_for_model(model)
    except KeyError:
        logging.error("Warning: model not found. Using cl100k_base encoding.")
        encoding = tiktoken.get_encoding("cl100k_base")
    if model == "gpt-3.5-turbo":
        return list_short_term_memory_messages_for_pruning(messages, max_tokens, model="gpt-3.5-turbo-0301")
    elif model == "gpt-4":
        return list_short_term_memory_messages_for_pruning(messages, max_tokens, model="gpt-4-0314")
    elif model == "gpt-3.5-turbo-0301":
        # every message follows <|start|>{role/name}\n{content}<|end|>\n
        tokens_per_message = 4
        tokens_per_name = -1  # if there's a name, the role is omitted
    elif model == "gpt-4-0314":
        tokens_per_message = 3
        tokens_per_name = 1
    else:
        raise NotImplementedError(
            f"""num_tokens_from_messages() is not implemented for model {model}. See https://github.com/openai/openai-python/blob/main/chatml.md for information on how messages are converted to tokens.""")

    def count_total_tokens(stm_token_list) -> int:
        num_tokens = 0
        for item in stm_token_list:
            num_tokens += tokens_per_message
            if item["stm"].role == "name":
                num_tokens += tokens_per_name
            num_tokens += item["num_tokens"]
        num_tokens += 3
        return num_tokens

    class STMTokenList(typing.TypedDict):
        stm: ShortTermMemory
        num_tokens: int

    stm_token_list: typing.List[STMTokenList] = [{"stm": stm, "num_tokens": len(
        encoding.encode(stm.content))} for stm in messages]

    stm_to_prune: typing.List[ShortTermMemory] = []
    while count_total_tokens(stm_token_list) > max_tokens:
        # remove first element and add it to prune list
        stm_to_prune.append(stm_token_list.pop(0)["stm"])

    return stm_to_prune
