import chromadb
from chromadb.config import Settings
from chromadb.utils.embedding_functions import OpenAIEmbeddingFunction

import streamlit as st

import os

import prompt

# Model, choose: "gpt-3.5-turbo", "gpt-3.5-turbo-16k" or "gpt-4", WARNING: GPT-4 can be quite expensive. Literally in $ en €.
# However... the new gpt-4-1106-preview is much cheaper than the 'old' gpt-4.
MODEL = "gpt-3.5-turbo"
MODEL_MAX_TOKENS = {
    "gpt-3.5-turbo": 16385,  # points to the latest gpt-3.5-turbo model
    "gpt-3.5-turbo-1106": 16385,
    "gpt-4": 8192,
    "gpt-4-1106-preview": 128000
}.get(MODEL, 4096)
MODEL_EMBEDDING = "text-embedding-3-small"
# MAX_TOKENS = number of token reserved for the LLM response (originally 500)
MAX_TOKENS = 750
SHORT_TERM_MEMORY_MAX_TOKENS = 1500

# TEMPERATURE
# The combination GPT3.5Turbo and temp 0.7 did not work well. TAKO drifted quite a lot.
# The combination GPT3.5Turbo en temp 0.2 works pretty well. Not as well as the combination GPT4 and temp=0.7, but acceptable.
# The combination GPT4 and temp=0.7 worked very well.
# Default = 0
TEMPERATURE = {
    "gpt-3.5-turbo": 0.2,
    "gpt-3.5-turbo-16k": 0.2,
    "gpt-3.5-turbo-1106": 0,
    "gpt-4": 0.7,
    "gpt-4-1106-preview": 0  # 0 seems to be working particularly well...
}.get(MODEL, 0)

# Use: export OPENAI_KEY="MyValue"
OPENAI_KEY = os.getenv('OPENAI_KEY', default="")


# The TAKO system prompt is tuned for different events and audiences.
# For example: teams of students in a hackathon can choose between different challenges.
# Then these challenges are mentioned in the system prompt.
PROMPT_STYLES = {
    "Rhizo april 2024": prompt.tako_system_message_basic,
    "Europe Direct 2024": prompt.tako_system_message_europedirect_2024,
    "Basis TAKO 2024": prompt.tako_system_message_basic,
}

HELLO_MESSAGE_USER = prompt.opening_message_user

# initialize vector db
# Open AI: "We recommend cosine similarity. The choice of distance function typically doesn’t matter much." https://platform.openai.com/docs/guides/embeddings/limitations-risks


@st.cache_resource
def get_chroma_client():
    return chromadb.PersistentClient(path="/app/data/chromadb")


@st.cache_resource
def get_chroma_collection(chroma_client, collection_name):
    embedding_function = OpenAIEmbeddingFunction(
        api_key=OPENAI_KEY, model_name=MODEL_EMBEDDING)

    return chroma_client.get_or_create_collection(
        name=collection_name,
        metadata={"hnsw:space": "cosine"},
        embedding_function=embedding_function)
