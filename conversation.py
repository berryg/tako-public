from sqlalchemy import Integer, String, Text, DateTime
from sqlalchemy.orm import DeclarativeBase
from sqlalchemy.orm import mapped_column
from sqlalchemy.sql import func


class Base(DeclarativeBase):
    pass


class Conversation(Base):
    __tablename__ = "conversation"

    id = mapped_column(Text, primary_key=True)
    team_name = mapped_column(String(50), nullable=False)
    tako_name = mapped_column(String(100), nullable=False)
    role = mapped_column(Text)
    content = mapped_column(Text)
    created_at = mapped_column(
        DateTime(timezone=True), server_default=func.now())


class ShortTermMemory(Base):
    __tablename__ = "short_term_memory"

    id = mapped_column(Text, primary_key=True)
    team_name = mapped_column(String(50), nullable=False)
    tako_name = mapped_column(String(100), nullable=False)
    role = mapped_column(Text)
    content = mapped_column(Text)
    created_at = mapped_column(
        DateTime(timezone=True), server_default=func.now())
