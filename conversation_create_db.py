from sqlalchemy import create_engine

from conversation import Base

if __name__ == "__main__":
    engine = create_engine('sqlite:////app/data/conversations.db')
    Base.metadata.create_all(engine)