#!/bin/bash
# Used by Dockerfile to load environment variables
if [ -f /app/config/env.list ]; then
    export $(cat /app/config/env.list | xargs)
fi

# Execute the main command
exec "$@"
