# these three lines swap the stdlib sqlite3 lib with the pysqlite3 package
__import__('pysqlite3')
import sys
sys.modules['sqlite3'] = sys.modules.pop('pysqlite3')

from datetime import datetime
from openai import OpenAI

import streamlit as st
import config
import uuid

from memory import Memory
from openai_tools import get_embedding, num_tokens_from_messages, list_short_term_memory_messages_for_pruning

from prompt import opening_message_user

from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from conversation import Conversation, ShortTermMemory

from pprint import pprint
import logging

client = OpenAI(api_key=config.OPENAI_KEY)


def get_system_msg(prompt_style):
    prompt = config.PROMPT_STYLES[prompt_style]["prompt"]
    
    return {"role": "system",
                  "content": prompt }

HELLO_MESSAGE_USER = config.HELLO_MESSAGE_USER


# Initialize connection to conversation DB
# Conversation DB contains the messages/questions of the user and the answers of TAKO
# Conversation DB also contains the short-term-memory messages for a team.
@st.cache_resource
def conversation_session():
    engine = create_engine('sqlite:////app/data/conversations.db')
    Session = sessionmaker(bind=engine)
    return Session()


def show_llm_response(message):
    # Get response from LLM and display it

    # Create a set of messages for the LLM. The set consists of:
    # 1. the system prompt (repeated with every message for the LLM), written in SudoLang, containing description of DT process and descriptions of canvasses/tools.
    # 2. long-term message (up until the token limit)
    # 3. short-term messages (up to 1500 tokens) including the current message from the user (last message)
    new_prompt = [get_system_msg(prompt_style)]

    @st.cache_resource
    def get_memory():
        return Memory(config.PROMPT_STYLES[prompt_style]["collection_name"])

    # Find long-term-memory messages associated with the current question of the user
    memory = get_memory()
    long_term_memory_messages = memory.search(
        get_embedding(message["content"]), team_name)

    # logging.error("Long Term Memory Messages...")
    # pprint(long_term_memory_messages)

    # Add short-term memory messages up to 1500 tokens
    short_term_messages = [{"role": "system", "content": f"Current time: {datetime.now().strftime('%B %d, %Y %I:%M:%S %p')}"},
                           {"role": "user", "content": message["content"]}
                           ]

    for msg in reversed([{"role": stm.role, "content": stm.content} for stm in conversation_session().query(
            ShortTermMemory).filter(ShortTermMemory.team_name == team_name).filter(ShortTermMemory.tako_name == prompt_style).order_by(ShortTermMemory.created_at).all()]):
        if num_tokens_from_messages(short_term_messages + [msg]) <= config.SHORT_TERM_MEMORY_MAX_TOKENS:
           short_term_messages.append(msg)
        else:
            break

    # Add long-term memory messages until the token limit is reached
    token_limit = config.MODEL_MAX_TOKENS - \
        config.SHORT_TERM_MEMORY_MAX_TOKENS - config.MAX_TOKENS
    # logging.info(f"token-limit: {token_limit}")

    for msg in sorted(long_term_memory_messages, key=lambda x: x["timestamp"]):
        if msg["insight"]:
            long_term_msg = [
                {"role": "system", "content": f"You had the following insight on {msg['timestamp'].strftime('%B %d, %Y %I:%M:%S %p')}: {msg['insight']}"}]
        else:
            long_term_msg = [{"role": "system", "content": f"This is a snippet from earlier on {msg['timestamp'].strftime('%B %d, %Y %I:%M:%S %p')}"},
                             {"role": "user", "content": msg["message"]},
                             {"role": "assistant", "content": msg["response"]}]

        if num_tokens_from_messages(new_prompt + short_term_messages + long_term_msg) <= token_limit:
            new_prompt.extend(long_term_msg)
        else:
            break

    new_prompt.extend(reversed(short_term_messages))

    # pprint(new_prompt)

    # get response from LLM using the new prompt and stream it to the UI
    with st.chat_message("assistant"):
        response = client.chat.completions.create(
            model=config.MODEL, messages=new_prompt, temperature=config.TEMPERATURE, max_tokens=config.MAX_TOKENS, stream=True)

        # this got streaming display going. See: https://docs.streamlit.io/library/api-reference/layout/st.empty
        message_placeholder = st.empty()
        full_response = ""
        for chunk in response:
            chunk_message = chunk.choices[0].delta.content
            if chunk_message != None:
                full_response += chunk_message
            message_placeholder.markdown(full_response)

    # Update short-term-memory
    with conversation_session() as s:
        conversation = Conversation(id=str(
            uuid.uuid4()), team_name=team_name, tako_name=prompt_style, role="assistant", content=full_response)
        stm_message = ShortTermMemory(id=str(uuid.uuid4(
        )), team_name=team_name, tako_name=prompt_style, role=message["role"], content=message["content"])
        stm_assistant = ShortTermMemory(id=str(uuid.uuid4(
        )), team_name=team_name, tako_name=prompt_style, role="assistant", content=full_response)
        s.add(conversation)
        s.add(stm_message)
        s.add(stm_assistant)
        s.commit()

    # Update long-term-memory using short-term-memory as input
    memory.upload_message_response_pair(
        message["content"], full_response, team_name)
    memory.reflect([{"role": stm.role, "content": stm.content} for stm in conversation_session().query(
        ShortTermMemory).filter(ShortTermMemory.team_name == team_name).filter(ShortTermMemory.tako_name == prompt_style).order_by(ShortTermMemory.created_at).all()], team_name)

    # Prune short term memory
    stm_to_prune = list_short_term_memory_messages_for_pruning(conversation_session().query(ShortTermMemory).filter(
        ShortTermMemory.team_name == team_name).filter(ShortTermMemory.tako_name == prompt_style).order_by(ShortTermMemory.created_at).all(), config.SHORT_TERM_MEMORY_MAX_TOKENS)

    with conversation_session() as s:
        for stm in stm_to_prune:
            s.delete(stm)
        s.commit()


def store_conversation_message(content):
    # Store user message in Conversation DB
    with conversation_session() as s:
        conversation = Conversation(
            id=str(uuid.uuid4()), team_name=team_name, tako_name=prompt_style, role="user", content=content)
        s.add(conversation)
        s.commit()


def get_conversations(team_name, prompt_style):
    return conversation_session().query(Conversation).filter(Conversation.team_name == team_name).filter(Conversation.tako_name == prompt_style).order_by(Conversation.created_at)


# MAIN STREAMLIT PROGRAM
with st.sidebar:
    st.title("🦑 TAKO")
    st.markdown(
        "TAKO is een AI Assistent die je helpt om met hulp van Design Thinking tot een mooie oplossing te komen voor een uitdaging."
    )
    if not config.OPENAI_KEY:
        config.OPENAI_KEY = st.text_input("OpenAI API Key", type="password")
    prompt_style = st.selectbox("Kies jouw TAKO", [v for v in config.PROMPT_STYLES], index=None, placeholder="Selecteer jouw Tako...")
    team_name = st.text_input("Team naam")
    st.markdown("# ")
    st.markdown("""
        *Versie: 14 april 2024* <br/> 
        *Model: {model}*
    """.format(model=config.MODEL), unsafe_allow_html=True)
    # st.markdown("*Model: {model}*".format(model=config.MODEL))
    st.image("images/eu_logo.png")


# Check if all required fields are filled in
if not (config.OPENAI_KEY and team_name and prompt_style != None):
    st.info("Vul in de grijze kolom links de volgende gegevens in om verder te gaan:", icon="ℹ️")
    if not config.OPENAI_KEY:
        st.info(" — Jouw OpenAI API key.", icon="🔑")
    if not prompt_style:
        st.info(" — Jouw TAKO die je gaat gaat begeleiden.", icon="🦑")
    if not team_name:
        st.info(" — Jullie Teamnaam.", icon="👥")
    st.stop()


# Show current conversation between user and TAKO stored in Conversation DB
for msg in get_conversations(team_name, prompt_style):
    if msg.role in ["assistant", "user"]:
        with st.chat_message(msg.role):
            st.markdown(msg.content)


# process initial message(s)
if conversation_session().query(Conversation).filter(Conversation.team_name == team_name).filter(Conversation.tako_name == prompt_style).count() == 0:
    st.chat_message("user").write(HELLO_MESSAGE_USER)
    store_conversation_message(HELLO_MESSAGE_USER)
    show_llm_response(message={"role": "user", "content": HELLO_MESSAGE_USER})

# process conversation
if prompt := st.chat_input(placeholder="Jouw bericht"):
    st.chat_message("user").write(prompt)
    store_conversation_message(prompt)
    show_llm_response(message={"role": "user", "content": prompt})
