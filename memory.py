from datetime import datetime
from openai_tools import get_embedding, get_importance_of_interaction, get_insights
import config

import chromadb
from chromadb.utils.embedding_functions import OpenAIEmbeddingFunction

import uuid
import numpy as np

import logging


class Memory():
    def __init__(self, collection_name):
        self.collection_name = collection_name
        
        # logging.info(f"Collection name: {collection_name}")

        self.chroma_client = chromadb.PersistentClient(path="/app/data/chromadb")

        embedding_function = OpenAIEmbeddingFunction(
            api_key=config.OPENAI_KEY, model_name=config.MODEL_EMBEDDING)

        self.collection = self.chroma_client.get_or_create_collection(
            name=self.collection_name,
            metadata={"hnsw:space": "cosine"},
            embedding_function=embedding_function)

    def upload_message_response_pair(self, message, response, team_name):
        importance = get_importance_of_interaction(message, response)
        self.collection.upsert(ids=[str(uuid.uuid4())], embeddings=[get_embedding(message + response)], metadatas=[
            {"team_name": team_name, "message": message, "response": response, "importance": importance, "timestamp": str(datetime.now())}])

    def reflect(self, messages, team_name):
        insights = get_insights(messages)

        # logging.info(f"Insights: {insights}")

        for insight in insights:
            self.collection.upsert(ids=[str(uuid.uuid4())], embeddings=[get_embedding(insight["content"])], metadatas=[
                {"team_name": team_name, "insight": insight["content"], "importance": insight["importance"], "timestamp": str(datetime.now())}])

    def search(self, vector, team_name, n=100):
        message_response_pairs = self.collection.query(
            query_embeddings=vector, where={"team_name": team_name}, n_results=1000)

        resultsA = [
            {
                "message": result["message"] if "message" in result else None,
                "response": result["response"] if "response" in result else None,
                "insight": result["insight"] if "insight" in result else None,
                "timestamp": datetime.fromisoformat(result["timestamp"]),
                "importance": result["importance"],
                "team_name": result["team_name"] if "team_name" in result else None,
            } for result in message_response_pairs["metadatas"][0]]

        resultsB = [
            {
                "similarity": result
            } for result in message_response_pairs["distances"][0]]

        results = [{**a, **b} for a, b in zip(resultsA, resultsB)]

        if results:
            # Compute the values for each dimension
            days_since_values = []
            importance_values = []
            similarity_values = []
            for result in results:
                days_since = (datetime.now() - result["timestamp"]).days
                days_since_values.append(np.exp(-0.99 * days_since))
                importance_values.append(result["importance"])
                similarity_values.append(result["similarity"])

            # Calculate the min and max values for each dimension
            min_days_since, max_days_since = min(
                days_since_values), max(days_since_values)
            min_similarity, max_similarity = min(
                similarity_values), max(similarity_values)

            # Calculate the min and max values for importance separately for insights and non-insights
            min_importance_insight, max_importance_insight = min(
                x["importance"] for x in results if x["insight"]), max(x["importance"] for x in results if x["insight"])
            min_importance_no_insight, max_importance_no_insight = min(
                x["importance"] for x in results if not x["insight"]), max(x["importance"] for x in results if not x["insight"])

            # Apply min-max scaling and compute the scaled score
            epsilon = 1e-8
            for i, result in enumerate(results):
                days_since_scaled = (
                    days_since_values[i] - min_days_since) / (max_days_since - min_days_since + epsilon)
                similarity_scaled = (
                    similarity_values[i] - min_similarity) / (max_similarity - min_similarity + epsilon)

                # Scale importance based on whether it was an insight or not
                if result["insight"]:
                    importance_scaled = (importance_values[i] - min_importance_insight) / (
                        max_importance_insight - min_importance_insight + epsilon)
                else:
                    importance_scaled = (importance_values[i] - min_importance_no_insight) / (
                        max_importance_no_insight - min_importance_no_insight + epsilon)

                result["score"] = 1/3 * days_since_scaled + 1 / \
                    3 * importance_scaled + 1/3 * similarity_scaled

            # Sort the results based on the score in descending order
            results.sort(key=lambda x: x["score"], reverse=True)

        # logString = "\n".join(r for r in results[:n])
        # logging.error(f"Search results:\n {logString}")

        # Return the top n results
        return results[:n]
