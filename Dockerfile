FROM python:3.9-slim

WORKDIR /app

RUN apt-get update && apt-get install -y \
    build-essential \
    curl \
    software-properties-common \
    git \
    && rm -rf /var/lib/apt/lists/*

# First copy only the requirements file to leverage Docker cache
COPY requirements.txt /app/requirements.txt
RUN pip3 install -r requirements.txt


# Copy the rest of the application code
COPY . .

# Copy the entrypoint script and make it executable
COPY entrypoint.sh entrypoint.sh
RUN chmod +x entrypoint.sh

EXPOSE 8501

HEALTHCHECK CMD curl --fail http://localhost:8501/_stcore/health

RUN mkdir -p /app/data
RUN python conversation_create_db.py

# Use the entrypoint script
ENTRYPOINT ["/app/entrypoint.sh"]

# Streamlit command as the default parameter to entrypoint.sh
CMD ["streamlit", "run", "tako.py", "--server.port=8501", "--server.address=0.0.0.0"]